import pymysql.cursors
import random
import numpy as np

connection = pymysql.connect(host='localhost',
                             user='root',
                             passwd='1111',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.Cursor)

#SHORTCUTS
#for insert and upd triggers
INSERTED_SUM = '(SELECT `Сумма к оплате` FROM `Итоговые цены` WHERE NEW.`id посещения` = `Итоговые цены`.`id посещения`)'
WORKER_SUM = '(SELECT `Сумма заработка` WHERE NEW.`id работника` = `id работника`)'

#for del trigger
DEL_SUM = '(SELECT `Сумма к оплате` FROM `Итоговые цены` WHERE OLD.`id посещения` = `Итоговые цены`.`id посещения`)'
WORKER_DEL_SUM = '(SELECT `Сумма заработка` WHERE OLD.`id работника` = `id работника`)'

#GENERETE PRESETS
workers = ['Никола Р.М.', 'Клара К.Т.', 'Максим В.П.', 'Стив Р.Г.']
workers_qualification = ['Первая', 'Вторая', 'Высшая']

animals_names = ['Дудка', 'Мурка', 'Кука', 'Сникерс', 'Кот', 'Собака', 'Вобла', 'Жёлудь', 'Туман', 'Вдова', 'MySQL',
                 'Питон', 'ИТ', 'Луна', 'Кличка', 'Овраг', 'Скрепка', 'Дракон', 'Тор', 'Зевс', 'Крон', 'Орфей']
names_of_owners = ['Влад К.П.', 'Тиньков В.М.', 'Втажин К.Б.', 'Талов Б.Р.', 'Таков Т.Б.', 'Морон К.А.', 'Топов М.Р.',
                   'Тиров В.А.', 'Косто М.В.', 'Тошин Б.А.', 'Киров М.В.', 'Типов К.В.', 'Тишин О.А.', 'Торин М.В.']
animals = {'Гекон': 'Рептилия', 'Жаба': 'Амфибия', 'Рыба-хирург': 'Рыба', 'Гну': 'Млекопитающее', 'Тарантул': 'Паук',
           'Собака': 'Млекопитающее', 'Вомбат': 'Млекопитающее', 'Бурундук': 'Млекопитающее', 'Орёл': 'Птицы',
           'Кабан': 'Млекопитающее', 'Морская свинка': 'Млекопитающее', 'Кролик': 'Млекопитающее',
           'Капибара': 'Млекопитающее', 'Коза': 'Млекопитающее', 'Панда': 'Млекопитающее', 'Хамелеон': 'Рептилия',
           'Коала': 'Млекопитающее', 'Cаламандра': 'Земноводное', 'Голубь': 'Птица', 'Бобёр': 'Млекопитающее'}
diseases = ['Конъюнктивит', 'Отит', 'Отравление', 'Клещи', 'Гельминтоз', 'Бешенство', 'Чумка']

NUMBER_OF_LOG_ENTRIES = 30
THE_MINIMUM_CHARGE_FOR_A_VISIT = 3000
THE_MAXIMUM_CHARGE_FOR_A_VISIT = 7000

try:
    cursor = connection.cursor()

    cursor.execute("CREATE DATABASE `Ветеринарная клиника`")

    cursor.execute("use `Ветеринарная клиника`")

    cursor.execute("CREATE TABLE `Работники` (\
                   `id работника` SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,\
                   `ФИО работника` CHAR(30) NOT NULL,\
                   `Квалификация` CHAR(30) NOT NULL, \
                   `Количество обращений` SMALLINT UNSIGNED NOT NULL DEFAULT 0,\
                   `Сумма заработка` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0\
                   ) ENGINE=InnoDB")

    cursor.execute("CREATE TABLE `Животные` (\
                   `id животного` SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY, \
                   `Кличка животного` CHAR(30) NOT NULL, \
                   `Название животного` CHAR(30), \
                   `Вид животного` CHAR(30), \
                   `ФИО владельца` CHAR(30), \
                   `Телефон` CHAR(16) \
                   ) ENGINE=InnoDB")

    cursor.execute("CREATE TABLE `Журнал посещений` ( \
                   `id посещения` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,\
                   `id работника` SMALLINT UNSIGNED,\
                   `id животного` SMALLINT UNSIGNED,\
                   `Диагноз` CHAR(30),\
                   `Дата посещения` DATE NOT NULL,\
                   `Стоимость посещения` SMALLINT UNSIGNED DEFAULT 100,\
                   `Стоимость лечения` MEDIUMINT UNSIGNED NOT NULL,\
                   FOREIGN KEY (`id работника`) REFERENCES `Работники` (`id работника`) ON DELETE SET NULL,\
                   FOREIGN KEY (`id животного`) REFERENCES `Животные` (`id животного`) ON DELETE SET NULL\
                   ) ENGINE=InnoDB")

    cursor.execute("CREATE TABLE `Итоговые цены` (\
                   `id посещения` SMALLINT UNSIGNED UNIQUE,\
                   `Сумма к оплате` MEDIUMINT UNSIGNED,\
                   FOREIGN KEY (`id посещения`) REFERENCES `Журнал посещений` (`id посещения`)\
                   ) ENGINE=InnoDB")

    cursor.execute("CREATE VIEW `Просмотр журнала` AS SELECT `Журнал посещений`.`id посещения`, \
                   `Работники`.`ФИО работника`, `Животные`.`Кличка животного`, `Животные`.`ФИО владельца`, \
                   `Журнал посещений`.`Диагноз`, `Журнал посещений`.`Дата посещения`, \
                   `Журнал посещений`.`Стоимость посещения`, `Журнал посещений`.`Стоимость лечения`, \
                   `Итоговые цены`.`Сумма к оплате` FROM `Журнал посещений`, `Работники`, `Животные`, `Итоговые цены` \
                   WHERE `Журнал посещений`.`id посещения` = `Итоговые цены`.`id посещения` \
                   AND `Журнал посещений`.`id работника` = `Работники`.`id работника` \
                   AND `Журнал посещений`.`id животного` = `Животные`.`id животного` GROUP BY `id посещения`")

    cursor.execute("CREATE TRIGGER `insert_trigger` AFTER INSERT ON `Журнал посещений` \
                    FOR EACH ROW BEGIN \
                        IF NEW.`id животного` IS NOT NULL THEN \
                            INSERT INTO `Итоговые цены` (`id посещения`, `Сумма к оплате`) VALUES \
                            (NEW.`id посещения`, NEW.`Стоимость посещения` + NEW.`Стоимость лечения`);\
                            UPDATE `Работники` SET `Количество обращений` = `Количество обращений` + 1, \
                            `Сумма заработка` = `Сумма заработка` + " + INSERTED_SUM + " \
                            WHERE `id работника` = NEW.`id работника`; \
                        END IF; \
                    END; ")

    cursor.execute("CREATE TRIGGER `del_trigger` BEFORE DELETE ON `Журнал посещений` \
                    FOR EACH ROW BEGIN \
                        UPDATE `Работники` SET `Количество обращений` = `Количество обращений` - 1, \
                        `Сумма заработка` = `Сумма заработка` - " + DEL_SUM + " \
                        WHERE `id работника` = OLD.`id работника`; \
                        DELETE FROM `Итоговые цены` WHERE `id посещения` = OLD.`id посещения`; \
                    END; ")

    cursor.execute("CREATE TRIGGER `upd_trigger` BEFORE UPDATE ON `Журнал посещений` \
                    FOR EACH ROW BEGIN \
                        UPDATE `Работники` SET `Сумма заработка` = `Сумма заработка` - \
                        " + INSERTED_SUM + " + NEW.`Стоимость посещения` + NEW.`Стоимость лечения` \
                        WHERE `id работника` = NEW.`id работника`; \
                        UPDATE `Итоговые цены` SET `Сумма к оплате` = NEW.`Стоимость посещения` + NEW.`Стоимость лечения`;\
                    END; ")

    for worker in workers:
        cursor.execute("INSERT INTO `Работники` (`ФИО работника`, `Квалификация`) VALUES (%s, %s)",
                       (worker, random.choice(workers_qualification)))

    for number_of_call in range(NUMBER_OF_LOG_ENTRIES):
        pregenerate_number_for_animal = random.randrange(len(animals))
        #ДОработай, тут должна быть проверка на совпадение. Может же быть так, что одно и то же животное
        cursor.execute("INSERT INTO `Животные` (`Кличка животного`, `Название животного`, `Вид животного`, `ФИО владельца`, \
                       `Телефон`) VALUES (%s, %s, %s, %s, %s)",
                       (random.choice(animals_names), list(animals)[pregenerate_number_for_animal],
                       animals.get(list(animals)[pregenerate_number_for_animal]), random.choice(names_of_owners),
                       '+7(' + str(random.randrange(800, 900)) + ')' + str(random.randrange(100, 1000)) + '-' +
                       str(random.randrange(10, 100)) + '-' + str(random.randrange(10, 100))))

        cursor.execute("INSERT INTO `Журнал посещений` (`id работника`, `id животного`, `Диагноз`,\
                        `Дата посещения`, `Стоимость лечения`) VALUES (%s, %s, %s, %s, %s)",
                        (random.randrange(1, len(workers) + 1), number_of_call + 1, random.choice(diseases),
                        '2018-' + str(random.randrange(1, 13)) + '-' + str(random.randrange(1, 28)),
                        random.randrange(THE_MINIMUM_CHARGE_FOR_A_VISIT, THE_MAXIMUM_CHARGE_FOR_A_VISIT)))

    connection.commit()

    cursor.execute("SELECT * FROM `Журнал посещений`")
    result = cursor.fetchall()
    cursor.execute("SELECT `id животного` FROM `Животные`")
    result_o = np.array(cursor.fetchall())
    print(result, result_o[:, 0])
finally:
    # with connection.cursor() as cursor:  #Раскомментируйте, если захотите побаловаться с генерацией В противном случае,
    #    cursor.execute("DROP DATABASE `Ветеринарная клиника`")  #чтобы сгенерировать БД, вам придётся удалять прошлую вручную
    connection.close()